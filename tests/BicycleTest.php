<?php
use Mayenok\Bicycle;
use PHPUnit\Framework\TestCase;

class BicycleTest extends TestCase
{
    /**
     * @var Bicycle $bike
     */
    protected $bike;

    /**
     * @var Bicycle $bike_with_big_wheels
     */
    protected $bike_with_big_wheels;

    /**
     * @var Bicycle $expensive_bike
     */
    protected $expensive_bike;

    protected function setUp()
    {
        $this->bike = new Bicycle(20, 1, new \Mayenok\CheapGear());
        $this->bike_with_big_wheels = new Bicycle(28, 1, new \Mayenok\CheapGear());
        $this->expensive_bike = new Bicycle(24, 1, new \Mayenok\ExpensiveGear());
    }

    public function testCreateBikeWithWrongGearLevel() {
        $this->expectException(InvalidArgumentException::class);
        $cheap_gear = new \Mayenok\CheapGear();
        new Bicycle(20, $cheap_gear->getMaxGearLevel()+1, $cheap_gear);
    }

    /**
     * @throws ErrorException
     */
    public function testIncreaseGear() {
        $previous_gear = $this->bike->getGearLevel();
        $this->bike->increaseGear();
        $this->assertTrue($previous_gear < $this->bike->getGearLevel());
    }

    /**
     * @throws ErrorException
     */
    public function testIncreaseHighestGear() {
        $this->expectException(ErrorException::class);
        $this->expectExceptionMessage('This is the highest gear level.');
        for ($i = 0; $i <= $this->bike->getGear()->getMaxGearLevel(); $i++) {
            $this->bike->increaseGear();
        }
    }

    /**
     * @throws ErrorException
     */
    public function testDecreaseGear() {
        $this->bike->increaseGear();
        $previous_gear = $this->bike->getGearLevel();
        $this->bike->decreaseGear();
        $this->assertTrue($previous_gear > $this->bike->getGearLevel());
    }

    /**
     * @throws ErrorException
     */
    public function testDecreaseLowestGear() {
        $this->expectException(ErrorException::class);
        $this->expectExceptionMessage('This is the lowest gear level.');
        $this->bike->decreaseGear();
    }

    /**
     * @throws ErrorException
     */
    public function testCycle() {
        // expensive bike at 1st gear level
        $this->assertEquals(2.256, $this->expensive_bike->cycle(1), '', 0.001);

        // cheap bike at 1st gear level
        $this->assertEquals(3.021, $this->bike->cycle(1), '', 0.001);

        // test number of rotations
        $this->assertLessThan($this->bike->cycle(2), $this->bike->cycle(1));

        // test wheel size
        $this->assertLessThan($this->bike_with_big_wheels->cycle(1), $this->bike->cycle(1));

        // test increase gear
        $prev_distance = $this->bike->cycle(10);
        $this->bike->increaseGear();
        $this->assertLessThan($this->bike->cycle(10), $prev_distance);
    }
}
