<?php

namespace Mayenok;


/**
 * Interface GearInterface
 * @package Mayenok
 */
interface GearInterface
{
    /**
     * Calculates sprocket teeth number according to formula
     * @param int $gear_level
     * @return int
     */
    public function SprocketTeethNum(int $gear_level): int;

    /**
     * @return int
     */
    public function getMaxGearLevel(): int;
}