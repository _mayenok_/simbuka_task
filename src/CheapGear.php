<?php

namespace Mayenok;


/**
 * Class CheapGear
 * @package Mayenok
 */
class CheapGear implements GearInterface
{
    const MAX_GEAR_LEVEL = 7;

    /**
     * @param int $gear_level
     * @return int
     */
    public function SprocketTeethNum(int $gear_level): int {
        return 30 - 2 * $gear_level;
    }

    /**
     * @return int
     */
    public function getMaxGearLevel(): int
    {
        return self::MAX_GEAR_LEVEL;
    }
}