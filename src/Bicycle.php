<?php

namespace Mayenok;


/**
 * Class Bicycle
 * @package Mayenok
 */
class Bicycle
{
    const INCH_TO_METER = 0.0254;
    const CHAINRING_TEETH_NUM = 53;
    private $gear_level;
    private $gear;
    private $circumference;
    private $sprocket_teeth_num;

    /**
     * Bicycle constructor.
     * @param int $wheel_size
     * @param int $gear_level
     * @param GearInterface $gear
     */
    public function __construct(int $wheel_size, int $gear_level, GearInterface $gear) {

        if ($gear_level < 1 || $gear_level > $gear->getMaxGearLevel()) {
            throw new \InvalidArgumentException('Invalid gear level');
        }
        $this->gear_level = $gear_level;
        $this->gear = $gear;
        $this->circumference = pi() * ($wheel_size * self::INCH_TO_METER);
        $this->updateSprocketTeethNum();
    }

    /**
     * @return int
     */
    public function getGearLevel(): int {
        return $this->gear_level;
    }

    /**
     * @return GearInterface
     */
    public function getGear(): GearInterface {
        return $this->gear;
    }

    /**
     * Increases the gear level with 1.
     * @throws \ErrorException
     */
    public function increaseGear(): void {
        if ($this->gear_level <= $this->gear->getMaxGearLevel()) {
            $this->gear_level++;
            $this->updateSprocketTeethNum();
        } else {
            throw new \ErrorException('This is the highest gear level.');
        }
    }

    /**
     * Decreases the gear level with 1.
     * @throws \ErrorException
     */
    public function decreaseGear(): void {
        if ($this->gear_level > 1) {
            $this->gear_level--;
            $this->updateSprocketTeethNum();
        } else {
            throw new \ErrorException('This is the lowest gear level.');
        }
    }

    public function updateSprocketTeethNum(): void {
        $this->sprocket_teeth_num = $this->gear->SprocketTeethNum($this->gear_level);
    }

    /**
     * Returns the length traveled in meters
     * @param int $number_of_rotations
     * @return float
     */
    public function cycle(int $number_of_rotations): float {
        return $number_of_rotations * $this->moving_distance();
    }

    /**
     * Returns moving distance after one wheel rotation
     * @return float
     */
    private function moving_distance(): float {
        return $this->circumference * $this->gear_ratio();
    }

    /**
     * Returns gear ratio
     * @return float
     */
    private function gear_ratio(): float {
        return self::CHAINRING_TEETH_NUM / $this->sprocket_teeth_num;
    }
}