<?php

namespace Mayenok;


/**
 * Class ExpensiveGear
 * @package Mayenok
 */
class ExpensiveGear implements GearInterface
{
    const MAX_GEAR_LEVEL = 30;

    /**
     * @param int $gear_level
     * @return int
     */
    public function SprocketTeethNum(int $gear_level): int {
        return 46 - floor(1.2 * $gear_level);
    }

    /**
     * @return int
     */
    public function getMaxGearLevel(): int
    {
        return self::MAX_GEAR_LEVEL;
    }
}